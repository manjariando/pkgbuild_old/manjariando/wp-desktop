# Maintainer: tioguda <guda.flavio@gmail.com>
#
# Contributor: ValHue <vhuelamo at gmail dot com>
#

_appname=wordpress.com
pkgname=wp-desktop
pkgver=6.13.0
pkgrel=1
pkgdesc="WordPress.com Desktop client"
url="https://desktop.wordpress.com"
arch=('x86_64')
license=('GPL2')
makedepends=('imagemagick')
conflicts=('wp-desktop-dev')
provides=("${pkgname}=${pkgver}")

source=("https://github.com/Automattic/wp-calypso/releases/download/v${pkgver}/${_appname}-linux-x64-${pkgver}.tar.gz"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://github.com/Automattic/${pkgname}/blob/develop/LICENSE.md")
sha256sums=('0743f7fa3e659fd94b24af721589fbbe57a4a8782728b586f2f46e72f9ff81e0'
            'ba86ce78a264bf7cd208f44c71abe6ab04f095cbb94aef0db499e1ffb88d4b64'
            '757927bae9a5314796645f7fac1e6ff4931aa93b86b02d5d73c599b6e95a8a66')

_wpcom_desktop="[Desktop Entry]
Name=WordPress.com
Comment=WordPress.com Desktop Client
Comment[pt_BR]=Cliente Desktop WordPress.com
Exec=wpcom
Icon=wpcom
Type=Application
StartupNotify=true
Categories=Development;Network;"

build() {
    cd "${srcdir}"
    echo -e "$_wpcom_desktop" | tee com.wpcom.desktop
}

package() {
    depends=('alsa-lib' 'gcc-libs' 'gconf' 'gtk2' 'libgpg-error' 'libxss' 'libxkbfile' 'libxtst' 'nss')

    cd "${srcdir}/${_appname}-linux-x64-${pkgver}"
    install -d ${pkgdir}/usr/share/wpcom
    mv * ${pkgdir}/usr/share/wpcom
    install -d ${pkgdir}/usr/bin

    cd "${srcdir}"
    install -Dm 644 com.wpcom.desktop "${pkgdir}/usr/share/applications/com.wpcom.desktop"
    install -Dm 644 LICENSE.md "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
    install -Dm 644 "com.${pkgname}.metainfo.xml" "$pkgdir/usr/share/metainfo/com.${pkgname}.metainfo.xml"

    # Fix and install desktop icons
    for size in 22 24 32 48 64 128 256 512; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${pkgdir}/usr/share/wpcom/resources/app/public_desktop/windows-tray-bubble.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/wpcom.png"
    done

    cd "${pkgdir}/usr/share/wpcom"
    install -Dm644 ./resources/app/public_desktop/app-logo.png ${pkgdir}/usr/share/pixmaps/wpcom.png
    ln -s /usr/share/wpcom/wpcom ${pkgdir}/usr/bin/wpcom

    # Archify permissions
    chmod 4755 "${pkgdir}/usr/share/wpcom/chrome-sandbox"
}
